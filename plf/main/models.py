from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User

from uuidfield import UUIDField


class Token(models.Model):
    id = UUIDField(auto=True, primary_key=True)
    tag = models.CharField(max_length=128, blank=False, null=False, default=None)
    lifetime = models.IntegerField(default=60*60*24)
    created = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"%s %s" % (self.tag, self.id)

    @property
    def namespace(self):
        return '.'.join(self.tag.split('.')[:-1])

    @property
    def locator(self):
        return self.tag.split('.')[-1]

    def get_absolute_url(self):
        urlname = ':'.join(self.tag.split('.')[0:2])
        return reverse(urlname, args=[self.id])

    def is_valid(self):
        return True

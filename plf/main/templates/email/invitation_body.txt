Hi!

{{sender.username}} has added you to {{board.name}} where you will find some
photos and other stuff that {{sender.username}} wants to share with you and other
friends.

Please, go to {{invitation_url}}
to select your username and password and check them!

PD. This email invalidates previous invitations. Only the last link works.

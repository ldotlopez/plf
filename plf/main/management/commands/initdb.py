from django.core.management.base import BaseCommand
from main.models import User
from boards.models import Board


class Command(BaseCommand):
    args = 'ble ble'
    help = 'Ble ble read code'

    def handle(self, *args, **options):
        u1 = User(email='luis@localhost.com', username='luis', is_superuser=True, is_staff=True)
        u1.set_password('aaa')
        u1.save()

        u2 = User(email='test@localhost.com', username='test', is_staff=False)
        u2.set_password('bbb')
        u2.save()

        b1 = Board(name='Board 1', owner=u1)
        b1.save()

        b2 = Board(name='Board 2', owner=u2)
        b2.save()
        b2.members.add(u1)

import re
from django import forms
from django.db.models import Q
from main.models import User


def _validate_password_pair(cleaned_data, p1, p2):
    p1v = cleaned_data.get(p1)
    p2v = cleaned_data.get(p2)
    return (p1v == p2v)


class InvitationForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    token = forms.CharField(widget=forms.HiddenInput)

    def clean_username(self):
        value = self.cleaned_data.get('username')
        if not re.match(r'^[a-z0-9\.\-_ ]+$', value):
            raise forms.ValidationError("Username must contain only lowercase letters, numbers, '.', '-', '_' and spaces")
        if not User.objects.filter(username=value).exists():
            return value.lower()
        else:
            raise forms.ValidationError("This username is already taken")

    def clean(self):
        cleaned_data = super(InvitationForm, self).clean()
        if not _validate_password_pair(cleaned_data, 'password', 'password2'):
            raise forms.ValidationError("Passwords didn't match")

        return cleaned_data


class RecoverForm(forms.Form):
    locator = forms.CharField(label='Username or email')

    def clean_locator(self):
        value = self.cleaned_data.get('locator').lower()
        if not value:
            raise forms.ValidationError("This field is required")

        try:
            User.objects.get(Q(email=value) | Q(username=value))
            return value
        except User.DoesNotExist:
            raise forms.ValidationError("Username or email doesn't exists")


class PasswordResetForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(PasswordResetForm, self).clean()
        if not _validate_password_pair(cleaned_data, 'password', 'password2'):
            raise forms.ValidationError("Passwords didn't match")

        return cleaned_data

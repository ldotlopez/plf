def site(request):
    from django.conf import settings
    from django.contrib.sites.models import Site

    return {'site': Site.objects.get(pk=settings.SITE_ID)}

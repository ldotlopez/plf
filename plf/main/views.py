from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth import login, authenticate

from main.models import User, Token
from main.forms import InvitationForm, RecoverForm, PasswordResetForm
from main.utils import send_mail, build_full_url


def get_token(uuid, ns):
    try:
        return get_object_or_404(Token, pk=uuid, tag__startswith=ns+'.')
    except Token.DoesNotExist:
        return None


def invitation(request, token):
    o = get_token(token, 'main.invitation')
    user = get_object_or_404(User, pk=o.locator)

    if request.method == 'POST':
        form = InvitationForm(request.POST)
    else:
        form = InvitationForm(initial={'token': token, 'email': user.email})

    if form.is_bound and form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.username = username
        user.is_active = True
        user.set_password(password)
        user.save()
        o.delete()

        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('front')

    else:
        return render(request, 'main/invite.html', {'form': form, 'token': token})


def reset(request, token):
    o = get_token(token, 'main.reset')
    user = get_object_or_404(User, pk=o.locator)

    form = PasswordResetForm(request.POST or None)
    if not form.is_valid():
        return render(request, 'main/reset.html', {
            'form': form,
            'user': user})

    return HttpResponse('Password changed')


def recover(request):
    form = RecoverForm(request.POST or None)
    if not form.is_valid():
        return render(request, 'main/recover.html', {'form': form})

    locator = form.cleaned_data.get('locator')

    u = User.objects.get(Q(email=locator) | Q(username=locator))

    token = Token(tag='main.reset.%s' % (u.id,))
    token.save()

    send_mail(u, 'reset', {
        'user': u,
        'reset_url': build_full_url(token.get_absolute_url())
    })

    return render_to_response('main/recover_done.html')


@csrf_exempt
def debug(request):
    if request.method == 'POST':
        import ipdb
        ipdb.set_trace()
    return render_to_response('main/debug.html')

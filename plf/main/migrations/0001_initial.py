# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Token'
        db.create_table(u'main_token', (
            ('id', self.gf('uuidfield.fields.UUIDField')(unique=True, max_length=32, primary_key=True)),
            ('tag', self.gf('django.db.models.fields.CharField')(default=None, max_length=128)),
            ('lifetime', self.gf('django.db.models.fields.IntegerField')(default=86400)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Token'])


    def backwards(self, orm):
        # Deleting model 'Token'
        db.delete_table(u'main_token')


    models = {
        u'main.token': {
            'Meta': {'object_name': 'Token'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('uuidfield.fields.UUIDField', [], {'unique': 'True', 'max_length': '32', 'primary_key': 'True'}),
            'lifetime': ('django.db.models.fields.IntegerField', [], {'default': '86400'}),
            'tag': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '128'})
        }
    }

    complete_apps = ['main']
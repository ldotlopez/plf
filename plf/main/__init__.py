from main.models import User


def monkeypatch():
    @property
    def _User_is_ghost(inst):
        return (inst.username == inst.email) and (inst.is_active is False)

    User.is_ghost = _User_is_ghost

monkeypatch()

from django.conf.urls import patterns, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

patterns = urlpatterns = patterns(
    '',
    url(r'^invitation/(?P<token>[a-zA-Z0-9]{32})', 'main.views.invitation', name='invitation'),
    url(r'^reset/(?P<token>[a-zA-Z0-9]{32})',    'main.views.reset', name='reset'),
    url(r'^recover/$',    'main.views.recover', name='recover'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'main/login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^debug/$', 'main.views.debug'),
)


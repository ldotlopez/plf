from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail as django_send_mail
from django.template.loader import render_to_string

from main.models import User


def build_full_url(path):
    if not path or not path[0] == '/':
        raise Exception('Invalid url path')

    return "%(proto)s://%(domain)s%(path)s" % ({
        'proto': 'https' if settings.HTTPS else 'http',
        'domain': Site.objects.get(pk=settings.SITE_ID).domain,
        'path': path
        })


def send_mail(who, what, params):
    params_ = params.copy()
    params_['site'] = Site.objects.get(pk=settings.SITE_ID)

    subject = render_to_string("email/%s_subject.txt" % (what,), params_)
    subject = subject.replace("\n", "")

    body = render_to_string("email/%s_body.txt" % (what,), params_) + \
        "\n-- \n" + \
        render_to_string(settings.EMAIL_SIGNATURE_TEMPLATE, params_)

    django_send_mail(subject, body, settings.EMAIL_SENDER, [who.email], fail_silently=False)


def get_user_or_create_ghost(email):
    u, created = User.objects.get_or_create(email=email, defaults={
        'username': email,
        'email': email,
        'is_active': False
        })
    return u


def is_email(s):
    if (not s) or ('@' not in s[1:-1]):
        return False

    (u, d) = s.split('@')
    if not all([u, d, '.' in d[:-2]]):
        return False

    return True

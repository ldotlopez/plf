from django.conf.urls import patterns, include, url

# static
from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^albums/', include('photocms.urls', namespace="photocms")),
    url(r'^boards/', include('boards.urls', namespace="boards")),
    url(r'^plf/', include('main.urls', namespace="main")),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', RedirectView.as_view(url=reverse_lazy('photocms:list')), name='front'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

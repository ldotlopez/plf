"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

import os
from django.test import TestCase
from photocms.models import User, Album, Item
from photocms.utils import FakeUploadedFile


def fake_uploaded_file(name):
    path = os.path.join(os.path.dirname(__file__), 'resources', name)
    return FakeUploadedFile(path)


class SimpleTest(TestCase):
    def setUp(self):
        self.u1 = User(username='user1', email='user1@localhost', is_active=True)
        self.u2 = User(username='user2', email='user2@localhost', is_active=True)
        self.u1.save()
        self.u2.save()

    def tearDown(self):
        self.u1.delete()
        self.u2.delete()

    def create_single_album(self):
        data = {
            'owner': self.u1,
            'name': 'test album',
            }
        a = Album(**data)
        a.save()

        return a

    def _test_fs_integrity(self, a):
        self.assertTrue(os.path.isdir(a.folder))
        self.assertTrue(os.path.isdir(os.path.dirname(a.archive_path)))
        self.assertTrue(os.path.isfile(a.archive_path))

    def test_create_album(self):
        a = self.create_single_album()

        self._test_fs_integrity(a)
        self.assertEqual(
            [(x.id, x.name) for x in Album.objects.all()],
            [(a.id, a.name)]
            )

    def test_update_album(self):
        a = self.create_single_album()
        a.update({'name': 'updated album'})
        a.save()

        self.assertEqual(
            Album.objects.get(pk=a.id).name,
            'updated album')
        self._test_fs_integrity(a)

        a.update({
            'name': 'test album',
            'files_added': [
                fake_uploaded_file('sample1.jpg'),
                fake_uploaded_file('sample2.jpg'),
                fake_uploaded_file('sample3.jpg')]
            })
        a.save()

        self._test_fs_integrity(a)
        self.assertEqual(
            len(Item.objects.filter(album=a)),
            3)

        fid = a.items.all()[1].id
        a.update({
            'files_removed': [fid]
            })
        self._test_fs_integrity(a)
        self.assertEqual(
            len(Item.objects.filter(album=a)),
            2)

    def test_destroy_album(self):
        a = self.create_single_album()
        a.delete()

        self.assertTrue(not os.path.isdir(a.folder))
        self.assertTrue(not os.path.isdir(os.path.dirname(a.archive_path)))

    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

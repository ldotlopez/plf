from django.contrib import admin
from photocms.models import Album, Item

admin.site.register(Album)
admin.site.register(Item)

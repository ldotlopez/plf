from django.conf.urls import patterns, url

from photocms import views

urlpatterns = patterns(
    '',
    url(r'^$', views.AlbumListView.as_view(), name='list'),
    url(r'^upload(/(?P<album_id>\d+))?', views.api_album_update, name='upload'),
    url(r'^detail/(?P<pk>\d+)/$', views.AlbumDetailView.as_view(), name='detail'),
    url(r'^share/(?P<album_id>\d+)$', views.api_album_update, name='share'),
    url(r'^(?P<album_id>\d+)/delete$', views.api_album_delete, name='delete'),

    # CRUD API
    #url(r'^_/update(/(?P<album_id>\d+))?', views.api_album_update, name='api_album_update'),
    #url(r'^_/list', views.api_album_list, name='api_album_list'),
    #url(r'^_/(?P<album_id>\d+)', views.api_album_detail, name='api_album_detail'),
)

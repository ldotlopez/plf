from django.conf import settings
from django.core.management.base import BaseCommand

from sorl.thumbnail import get_thumbnail

from main.models import User

from photocms import get_profiled_thumbnail
from photocms.models import Item


class Command(BaseCommand):
    args = ''
    help = 'Rebuilds all thumbnails'

    def handle(self, *args, **options):
        for i in Item.objects.all():
            for profile in settings.PHOTOCMS_THUMBS.keys():
                get_profiled_thumbnail(i.file, profile)

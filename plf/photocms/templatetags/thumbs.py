from django import template
from photocms import get_profiled_thumbnail

register = template.Library()

@register.filter
def thumb(f, profile):
    return get_profiled_thumbnail(f, profile).url

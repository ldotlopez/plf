from hashlib import sha1
from os import makedirs, rmdir, unlink, listdir
from os.path import join, basename, dirname, splitext
from random import sample
from re import subn
from time import mktime
from zipfile import ZipFile

from django.core.files.storage import default_storage
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver


from sorl.thumbnail import ImageField, delete as sorl_delete


from main.models import User
from boards.models import Board
from photocms import get_profiled_thumbnail


class Album(models.Model):
    name = models.CharField(max_length=255, null=False, default=None, blank=False)
    pub_date = models.DateTimeField('date published', auto_now=True)
    last_update = models.DateTimeField('last update', auto_now=True)
    owner = models.ForeignKey(User, related_name='albums', null=False, on_delete=models.CASCADE)
    boards = models.ManyToManyField(Board, related_name='albums', null=False)
    updated = True

    # Django methods

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('photocms:detail', args=[self.id])

    # Extra properties

    # FIXME: Find better way
    @property
    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'owner': self.owner.id,
            'owner_username': self.owner.username,
            'archive': self.archive_url,
            'cover': self.cover.file.url if self.cover else '',
            'url': self.get_absolute_url(),
            'pub_date': mktime(self.pub_date.utctimetuple()),
            'items': [i.as_dict for i in self.items.all()]
            }

    @property
    def folder(self):
        return default_storage.path(join('photos', str(self.id)))

    @property
    def archive_name(self):
        return 'zips/' + str(self.id) + '/' + subn(r'[^a-zA-Z0-9_ \-]', '_', self.name)[0] + ".zip"

    @property
    def archive_path(self):
        return default_storage.path(self.archive_name)

    @property
    def archive_url(self):
        return default_storage.url(self.archive_name)

    @property
    def cover(self):
        try:
            return self.items.all()[0]
        except IndexError:
            return None

    #
    # get_cover and get_sample are methods and not properties because they
    # can return (and probably they will) diferent results
    #

    def get_cover(self):
        self.get_sample()

        try:
            return self._sample[0]
        except IndexError:
            return None

    def get_sample(self, m=4):
        try:
            return self._sample[1:]

        except AttributeError:
            items = self.items.all()
            if len(items) < m+1:
                self._sample = (items[0],)
            else:
                self._sample = sample(items, m+1) 

            return self._sample[1:]

    #
    # Update for CRUD
    #
    def update(self, data):
        name = data.get('name')
        if name:
            self.updated = True
            self.name = name

        files_added = data.get('files_added')
        if files_added:
            for upload in files_added:
                Item(album=self, file=self._handle_file(upload)).save()
            self.updated = True

        files_removed = data.get('files_removed')
        if files_removed:
            for item in Item.objects.filter(pk__in=files_removed, album=self):
                item.delete()
            self.updated = True

    #
    # Init and cleanup filesystem
    #
    def init_fs(self):
        makedirs(self.folder)
        makedirs(dirname(self.archive_path))

    def cleanup_fs(self):
        # Remove album folder
        rmdir(self.folder)

        # Remove zip stuff
        zipdir = dirname(default_storage.path(self.archive_name))
        for f in [zipdir+"/"+e for e in listdir(zipdir)]:
            unlink(f)
        rmdir(zipdir)

    #
    # Checking access
    #
    def user_can_access(self, user):
        if user.is_anonymous():
            return False

        if self.owner == user:
            return True

        album_set = set(self.boards.all())
        user_set = set(user.boards.all())

        if album_set.intersection(user_set):
            return True
        else:
            return False

    def user_can_edit(self, user):
        if user.is_anonymous():
            return False

        if self.owner == user:
            return True

    #
    # Other methods
    #
    def create_archive(self):
        zipdirname = dirname(self.archive_path)
        for f in [zipdirname + "/" + e for e in listdir(zipdirname)]:
            unlink(f)

        zip = ZipFile(self.archive_path, 'w')
        for (i, item) in enumerate(self.items.all()):
            zip.write(item.file.path, "%s/%04d_%s" % (self.name, i+1, basename(item.file.name)))
        zip.close()


def _upload_handler(item, filename):
    digest = sha1()
    for chunk in item.file.chunks():
        digest.update(chunk)

    hexdig = digest.hexdigest()
    return default_storage.get_available_name(
        'photos/%d/%s%s' % (item.album.id, hexdig, splitext(filename)[1])
        )


class Item(models.Model):
    album = models.ForeignKey('Album', related_name='items', null=False, on_delete=models.CASCADE)
    file = ImageField(upload_to=_upload_handler, null=False)

    # FIXME: Find better way
    @property
    def as_dict(self):
        return {
            'id': self.id,
            'url': self.file.url,
            }

    @property
    def thumbnail(self):
        return get_profiled_thumbnail(self.file, 'square')

    @property
    def medium(self):
        return get_profiled_thumbnail(self.file, 'medium')


@receiver(post_save, sender=Album)
def album_post_save(sender, **kwargs):
    album = kwargs.get('instance')
    created = kwargs.get('created')

    if created:
        album.init_fs()

    if album.updated:
        album.create_archive()

    album.updated = False


@receiver(pre_delete, sender=Album)
def album_pre_delete(sender, **kwargs):
    kwargs.get('instance').cleanup_fs()


@receiver(pre_save, sender=Item)
def item_pre_save(sender, **kwargs):
    item = kwargs.get('instance')

    if not item.file:
        raise Exception("file can't be null")


@receiver(pre_delete, sender=Item)
def item_pre_delete(sender, **kwargs):
    sorl_delete(kwargs.get('instance').file)

from django.conf import settings
from sorl.thumbnail import get_thumbnail

def get_profiled_thumbnail(f, profile_name):
    params = settings.PHOTOCMS_THUMBS[profile_name]
    args = []
    kwargs = {}
    for p in params:
        if not isinstance(p, dict):
            args.append(p)
        else:
            kwargs.update(p)

    return get_thumbnail(f, *args, **kwargs)

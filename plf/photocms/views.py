import json
import datetime

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from photocms.models import Album, Item


from django.views.generic import ListView
from django.views.generic.detail import DetailView

import logging

_logger = logging.getLogger('plf.user')


class AlbumListView (ListView):
    model = Album

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        _logger.info('%s:photocms.index' % (self.request.user,))
        return super(AlbumListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Album.objects.filter(boards__in=self.request.user.boards.all()).order_by('-pub_date').distinct()

    # Handle AJAX as well?
        #ret = {
        #    'code': '0',
        #    'msg': 'success',
        #    'albums': []
        #}
        #
        #for album in albums:
        #    ret['albums'].append({
        #        'id': album.id,
        #        'url': album.get_absolute_url(),
        #        'name': album.name,
        #        'cover': album.cover.file.url if album.cover else '',
        #        'nitems': len(album.items.all())
        #        })
        #
        #return HttpResponse(json.dumps(ret), mimetype='application/json')


class AlbumDetailView(DetailView):
    model = Album

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        _logger.info('%s:photocms.detail.%s %s' % (self.request.user, kwargs['pk'], self.get_object()))
        return super(DetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AlbumDetailView, self).get_context_data(**kwargs)

        user = self.request.user
        album = kwargs['object']
        if not album.user_can_access(user):
            raise PermissionDenied

        operations = []
        if album.user_can_access(user):
            operations.append('access')
        if album.user_can_edit(user):
            operations.append('edit')
        if album.owner == user:
            operations = ('access', 'edit', 'all')

        context.update({
            'operations': operations
            })
        return context

    # Handle ajax
        #return HttpResponse(json.dumps({
        #    'code': 0,
        #    'msg': 'ok',
        #    'operations': operations,
        #    'album': album.as_dict
        #}), mimetype='application/json')


#
# CRUD: Update
#
@login_required
@csrf_exempt
def api_album_update(request, album_id):
    def _is_valid_email(s):
        if (not s) or ('@' not in s[1:-1]):
            return False

        (u, d) = s.split('@')
        if not all([u, d, '.' in d[:-2]]):
            return False

        return True

    if request.method == 'POST':
        opts = {}
        opts['id'] = album_id
        opts['name'] = request.POST.get('name')
        opts['files_added'] = request.FILES.getlist('files_added') or []
        opts['grant_access'] = request.POST.getlist('grant_access') or []

        updated = False
        rebuild_archive = True

        if opts['id']:
            album = get_object_or_404(Album, pk=opts['id'])
        else:
            album = Album(owner=request.user, name=(opts['name'] or 'Untitled'))
            album.save()  # Needed to get the id
            updated = True
            rebuild_archive = False

        if opts['name'] and opts['name'] != album.name:
            album.name = opts['name']
            updated = True
            rebuild_archive = False

        iids = []
        for upload in opts['files_added']:
            item = Item(album=album, file=upload)
            item.save()
            iids.append(item.id)
            updated = True
            rebuild_archive = False


        # FIXME: There are better ways to do this

        # Filter input parameter to adjust to user boards
        boards = request.user.boards.filter(pk__in=request.POST.getlist('boards'))
        b_add = [b for b in boards if b not in album.boards.all()]
        b_del = [b for b in album.boards.all() if b not in boards]

        for b in b_add:
            album.boards.add(b)
        for b in b_del:
            album.boards.remove(b)


        if updated:
            album.last_update = datetime.datetime.now()
            album.save()

        if rebuild_archive:
            album.create_archive()

        if request.is_ajax():
            return HttpResponse(json.dumps({
                'code': '0',
                'msg': 'success',
                'items_added': iids,
                'album': album.as_dict
                }), mimetype='application/json')
        else:
            return HttpResponseRedirect(reverse('photocms:detail', args=(album.id,)))

    else:
        return render(request, "photocms/upload.html")


#
# CRUD: Delete
#

@login_required
# @csrf_exempt
def api_album_delete(request, album_id):
    if request.method != 'POST':
        return HttpResponse(json.dumps({'code': '1', 'msg': 'Only POST allowed'}), mimetype='application/json')

    album = get_object_or_404(Album, pk=album_id)
    aid = album.id
    album.delete()

    if request.is_ajax():
        return HttpResponse(json.dumps({
            'code': '0',
            'msg': 'success',
            'id': aid,
        }), mimetype='application/json')

    else:
        return HttpResponseRedirect(reverse('photocms:list'))

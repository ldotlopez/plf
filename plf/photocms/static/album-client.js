Album = function (id) {
	this.id = id;
	this.isReady = false;
}


Album.prototype.init = function() {
	var self = this;

	$.ajax('/albums/_/' + id, {
		'success' : $.proxy(self._onSuccess, self),
		'error' : $.proxy(self._onError, self)
	});

	return self;
}

Album.prototype.itemizeItem = function(item) {
	var html = '' +
		'<div class="album-item" id="album-item-'+item.id+'">' +
		'	<a data-ajax="false" href="'+item.url+'"><img src="'+item.thumbnail+'" /></a>' +
		'</div>';

	return $(html);
}

Album.prototype._onSuccess = function (resp, status, xhr) {
	var self = this;

	var album = resp.album;

	$('.album .album-cover-wrap').html(' <img src="'+album.cover+'" /> ');

	$('.album h2').html(album.name);
	$('.album .album-owner').html('User no.'+album.owner);

	cycle = ['ui-block-a', 'ui-block-b','ui-block-c','ui-block-d'];
	$.each(album.items, function (index, item) {
		var obj = self.itemizeItem(item);
		obj.addClass(cycle[index%4]);
		$('.album-items').append(obj);
	});

	var nops = resp.operations.length;
	var gridType = 'ui-grid-' + ['a', 'b', 'c'][nops - 1];
	var blockCycle = ['a', 'b', 'c', 'd'].slice(0, nops);
	$.each(blockCycle, function(idx, e) {
		blockCycle[idx] = 'ui-block-' + e;
	});

	if (nops) { $('.album-operations').show(); }

	if (resp.operations.indexOf('download') >= 0) 
		$('.album-op-download').show();
/*

	if (resp.operations.indexOf('edit') >= 0) 
		$('.album-operations').append($('<a data-role="button" href="'+album.archive+'">Download</a>'));

	console.log(blockCycle);
	*/
	self.isReady = true;
}

Album.prototype._onError = function () {
	console.log("Error");
	console.log(arguments);
}

function Album (opts) {
	var need = ['baseUrl', 'endPoint', 'listView'];
	need.forEach(function (opt) {
		if (!opts.hasOwnProperty(opt)) {
			throw "Option "+opt+" is mandatory";
		}
	});

	this.baseUrl  = opts.baseUrl;
	this.endPoint = opts.endPoint;
	this.listView = opts.listView;
}

Album.prototype._createAlbumPreview = function(album) {
	return '' +
		'<li class="album-preview">'                   +
			'<a data-ajax="false" href="'+this.baseUrl+'/'+album.id+'">'           +
				'<img width="128px" class="album-cover" src="'+album.cover+'" />'   +
				'<h2>'+album.name+'</h2>'              +
				'<span class="ui-li-count">'+album.nitems+' items</span>' +
			'</a>'                                    +
		'</li>';
}


Album.prototype._onItemClick = function(ev) {
	console.log(ev);
}

Album.prototype.attachAlbum = function (album) {
	var item = $(this._createAlbumPreview(album));
	item.find('a').bind('click', this._onItemClick);

	this.listView.append(item);
}

Album.prototype.showPage = function (n) {
	console.warn("Argument n (n-page) is ignored for now");

	var me = this;
	var url = this.endPoint + '/list';

	$.ajax({
		'url': url,
		'success' : function (data, status, xhr) {
			data.albums.forEach(function (album) { me.attachAlbum(album); });
			me.listView.listview("refresh");
		}
	});
}


function humanizeSize(size, prefix) {
	table = ['', 'kb', 'mb', 'gb'];

	prefix = prefix || ''
	idx = table.indexOf(prefix);

	if (size < 1000 || (idx+1) == table.length)
		return size + ' ' + prefix;
	else
		return humanizeSize(Math.round(size/1000), table[idx + 1]);
}


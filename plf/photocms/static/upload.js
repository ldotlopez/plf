function Upload (opts) {
	if (typeof(opts) === "undefined") {
		throw "Options missing";
	}

	need = [
		'endPoint',          // API endpoint
		'fileFieldsWrapper', // For input type=file fields
		'listView',          // For progress view and feedback
	];
	need.forEach(function (opt) {
		if (!opts.hasOwnProperty(opt)) {
			throw "Option "+opt+" is mandatory";
		}
	});

	this.endPoint          = opts.endPoint;
	this.listView          = opts.listView;
	this.fileFieldsWrapper = opts.fileFieldsWrapper;

	this.itemizeFile       = opts.itemizeFile; // Optional callback for rendering items in listView
	this.onLoadFilesBegin  = opts.onLoadFilesBegin || function () {};
	this.onLoadFilesEnd    = opts.onLoadFilesEnd || function () {};
	this.onProgress        = opts.onProgress;
	this.onItemCompleted   = opts.onItemCompleted;
	this.onUploadCompleted = opts.onUploadCompleted;
	this.onFormBuild       = opts.onFormBuild || function () {};

	this.album = undefined;

	this.map = [];
}

Upload.prototype.init = function () {
	this.insertFileField();
	return this;
}

Upload.prototype.cleanup = function () {
	this.nameField.val('');
	this.fileFieldsWrapper.children().remove();
	this.listView.children().remove();

	this.map = [];
	this.album = undefined;
	// FIXME: Cancel uploads

	return this;
}

Upload.prototype.insertFileField = function (fieldId) {
	var self = this;
	var fieldId = fieldId || 0;

	// Build HTML for file field and his wrapper
	var wrap = $('<div id="wrap-files-'+fieldId+'"></div>');
	var field = $('<input type="file" name="files" id="files-'+fieldId+'" multiple="true" accept="image/*">');
	wrap.append(field);

	// Connect 'click' and 'change' events for feedback
	field.click(function (e) {
		self.onLoadFilesBegin();
	});

	field.change(function (e) {
		self.createTextFeedback(wrap);
		self.onLoadFilesEnd();
		self.insertFileField(fieldId + 1);
	});

	this.fileFieldsWrapper.children().removeClass('current-filefield');
	wrap.addClass('current-filefield');

	this.fileFieldsWrapper.append(wrap).trigger('create');
}

// FIXME: Be more generic (DnD in the future?)
Upload.prototype.createTextFeedback = function (wrap) {
	var self = this;

	var fileField = $(wrap).find('input[type="file"]');

	$.each(fileField[0].files, function(index, file) {
		var listViewItem = undefined;
		if (self.itemizeFile)
			listViewItem = self.itemizeFile(file);
		else
			listViewItem = $('<li class="ui-li-static">'+file.name+'</li>');

		self.saveMap(file, listViewItem);
		self.listView.append(listViewItem);
	});

	self.listView.listview('refresh');
}

/*
 * Functions to map files and listViewItems
 */
Upload.prototype.saveMap = function(file, listViewItem) {

	function getRandom() {
		return Math.floor(Math.random() * (10000 - 0 + 1) + 0);
	}

	var self = this;

	// Create unique ID for this item
	// FIXME: This ID generator is a joke, write a better one
	var found = true;
	var itemId = undefined;
	while (found) {
		itemId = 'upload-item-' + getRandom();
		found = self.listView.find('#' + itemId).length > 0;
	}

	listViewItem.attr('id', itemId);
	self.map.push({'file': file, 'itemId': itemId});

}

Upload.prototype.getListViewItemForFile = function (file) {
	var self = this;

	var n = self.map.length;
	for (var i = 0; i < n; i++) {
		var m = self.map[i];

		if (m.file != file)
			continue;

		var listViewItem = self.listView.find('#' + m.itemId);
		if (listViewItem.length)
			return listViewItem;
	}

	throw "Invalid file "+file.name+" ("+file.size+")";
}

Upload.prototype.getFileForListViewItem = function(listViewItem) {
	var self = this;

	var n = self.map.length;
	for (var i = 0; i < n; i++) {
		var m = self.map[i];

		if (m.itemId != listViewItem.attr('id'))
			continue;

		return m.file;
	}
	throw "Invalid listview item "+listViewItem.attr('id');
}

/*
 * Callbacks
 */
Upload.prototype.onAjaxSuccess = function (resp, status, xhr) {
	var self = this;

	var listViewItem = self.getListViewItemForFile (self.currFile);

	if ((status == 'success') && (resp.code == '0')) {
		console.log('Uploading file '+self.currFile.name+ ' done');

		// Update album info
		self.album = resp.album;
		listViewItem.addClass('upload-done');
	}
	else {
		console.log("Error with "+file.name+": "+status+", "+resp);

		listViewItem.addClass('upload-failed');
		// FIXME: show some message
	}

	self.ajaxCall = undefined;
	self.currFile = undefined;

	self.upload();
}

/*
 * Main method
 */
Upload.prototype.upload = function () {
	// Gets the next not uploaded file from the list view.
	// Listview is the reference for files, not the input fields
	function getNextFile () {
		var ret = self.listView.find('li:not(.upload-done)').first();
		if (!ret.length)
			return undefined;
	
		return self.getFileForListViewItem(ret);
	}

	var self = this;
	if (self.ajaxCall) {
		console.warn("Upload already in progress, ignoring call");
		return;
	}

	// Once all files are uploaded run the onUploadCompleted method
	self.currFile = getNextFile();
	if (!self.currFile) {
		if (self.onUploadCompleted)
			self.onUploadCompleted();
		return;
	}

	// Build POST URL
	var url = self.endPoint + (self.album ? '/' + self.album.id : '');

	// Build form
	var form = new FormData();
	form.append('files_added', self.currFile);
	self.onFormBuild(form);

	// Do POST
	console.log('Uploading file '+self.currFile.name);
	self.ajaxCall = $.ajax({
		url: url,
		type: 'POST',
		data: form,
		cache: false,
		xhr: function() {  // custom xhr
			myXhr = $.ajaxSettings.xhr();
			// check if upload property exists
			if (myXhr.upload) {
				myXhr.upload.addEventListener('progress', function(ev) {
					self.onProgress(ev, self.currFile)
				}, false);
			}
			else {
				console.log('upload progress not available');
			}
			return myXhr;
		},
		success: $.proxy(self.onAjaxSuccess, self),
		contentType: false,
		processData: false
	});
}


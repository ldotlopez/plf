	var currId = 0;

	function drawImgToCanvas(img, canvas) {
		var src_x; var src_y;
		var src_w; var src_h;

		var min_ = Math.min(img.width, img.height);
		if (img.width > min_) {
			src_x = (img.width - min_) / 2;
			src_y = 0;
			src_w = min_;
			src_h = img.height;
		}
		else {
			src_x = 0;
			src_y = (img.height - min_) / 2;
			src_w = img.width;
			src_h = min_;
		}

		var ctx = canvas.getContext('2d');
		ctx.drawImage(img,
			src_x, src_y, 
			src_w, src_h,
			0, 0,
			canvas.width, canvas.height);
		ctx.drawImage(ctx.canvas, 0, 0); 
	}

	function transformFileWrap(w) {
		w.find('label').remove();
		w.children().hide();
		$('#upload-status').html('');

		var input_ = w.find('input')[0];
		for (var i = 0; i < input_.files.length; i++ ) {
			var reader = new FileReader();

			reader.onload = function (e) {
			
				var canvas = $('<canvas>').attr('width', '64px').attr('height', '64px');
				w.append(canvas);

				var img = new Image();
				img.src = e.target.result;
				img.onload = function () {
					drawImgToCanvas(img, canvas[0]);
					$(img).remove();
			   	};
			}
			reader.readAsDataURL(input_.files[i]);
			delete reader;
		}

		insertFileField();
	}
	*/
/*
	function createTextFeedback(wrap) {
		var fileField = $(wrap).find('input[type="file"]');
		var listView = $('#upload-file-list');

		var nfiles = fileField[0].files.length;
		for (var i = 0; i < nfiles; i++) {
			var name = fileField[0].files[i].name;
			listView.append($('<li>'+name+'</li>'));
		}
		listView.trigger('create');
	}

	function setState(msg) {
		$('#upload-status').html(msg);
	}

	function insertFileField(n) {
		// var label = $('<label for="files-'+n+'">Select files from your device</label>');
		var field = $('<input type="file" name="files" id="files-'+n+'" multiple="true" accept="image/*">');
		var wrap = $('<div id="wrap-files-'+n+'"></div>');

		// field.change(function (e) { transformFileWrap(wrap) });
		field.click(function (e) {
			setState('Loading files, please wait…');
		});

		field.change(function (e) {
			createTextFeedback(wrap);
			setState('');
			wrap.hide();
			insertFileField(n+1);
		});

		// wrap.append(label);
		wrap.append(field);
		
		$('#upload-file-elements')
			.append(wrap)
			.trigger('create');
	}

	insertFileField(0);
	*/
});


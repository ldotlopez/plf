import os
from django.core.files import File
from django.core.files.uploadedfile import UploadedFile

class FakeUploadedFile(UploadedFile):
    def __init__(self, path):
        st = os.stat(path)
        name = os.path.basename(path)
        super(FakeUploadedFile, self).__init__(file(path), size=st.st_size, name=name)


from django.core.urlresolvers import reverse
from django.db import models


from main.models import User
from main.utils import get_user_or_create_ghost


class Board(models.Model):
    name = models.CharField(max_length=128, blank=False, null=False, default=None)
    owner = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    members = models.ManyToManyField(User, related_name='boards')

    def __unicode__(self):
        return u"%s" % (self.name,)

    def get_absolute_url(self):
        return reverse('boards:detail', args=(self.id,))

    def is_owner(self, user):
        return self.owner == user

    def is_member(self, user):
        try:
            self.members.get(pk=user.id)
            return True
        except User.DoesNotExist:
            return False

    def save(self, *args, **kwargs):
        if not hasattr(self, 'members'):
            super(Board, self).save(*args, **kwargs)

        self.members.add(self.owner)
        super(Board, self).save(*args, **kwargs)

    def add_members_from_emails(self, emails):
        for u in [get_user_or_create_ghost(x) for x in emails]:
            self.members.add(u)

    def set_members_from_emails(self, emails):
        self.members = [get_user_or_create_ghost(x) for x in emails]

    def remove_members_from_ids(self, ids):
        for m in self.members.filter(id__in=ids):
            self.members.remove(m)

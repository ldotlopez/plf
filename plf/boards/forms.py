import re

from django import forms
from django.forms import ModelForm

from main.utils import is_email

from boards.models import Board

class BoardForm(forms.Form):
    name = forms.CharField()
    members = forms.CharField(widget=forms.widgets.Textarea())

    def clean_members(self):
        value = self.cleaned_data.get('members')
        return [x for x in re.split(r'[\n\r,\s]+', value) if is_email(x)]

class BoardModelForm(ModelForm):
    class Meta:
        model = Board
        fields = ['name', 'members']

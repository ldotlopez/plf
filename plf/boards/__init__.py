from django.db.models.signals import m2m_changed, post_save, pre_save
from django.core.urlresolvers import reverse
from django.dispatch import receiver

from main.models import User, Token
from main.utils import send_mail, build_full_url

from boards.models import Board
from photocms.models import Album

@receiver(m2m_changed)
def board_m2m_changed(*args, **kwargs):
    board = kwargs.get('instance')
    action = kwargs.get('action')

    if isinstance(board, Board) and action == 'post_add':
        users = User.objects.filter(pk__in=kwargs.get('pk_set'))
        users = [x for x in users if x.is_ghost]
        for usr in users:
            tok = Token(tag='main.invitation.'+str(usr.id))
            tok.save()

            send_mail(usr, 'invitation', {
                'user': usr,
                'sender': board.owner,
                'board': board,
                'invitation_url': build_full_url(reverse('main:invitation', args=[tok.id])),
            })



@receiver(m2m_changed)
def album_m2m_changed(*args, **kwargs):
    album = kwargs.get('instance')
    action = kwargs.get('action')

    if isinstance(album, Album) and action == 'post_add':
        for usr in User.objects.filter(boards__in=kwargs.get('pk_set')).exclude(pk=album.owner.id):
            send_mail(usr, 'new_album', {
                'user': usr,
                'album': album,
                'sender': album.owner,
                'album_url': build_full_url(reverse('photocms:detail', args=[album.id])),
            })

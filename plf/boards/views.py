import re

from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect

from django.utils.decorators import method_decorator

from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView, ListView
from django.views.generic.detail import DetailView


from main.utils import is_email
from boards.models import Board
from boards.forms import BoardForm


# FIXME: This should work as a mixin?
#class ProtectedView(TemplateView):
#    @method_decorator(login_required)
#    def dispatch(self, *args, **kwargs):
#        return super(ProtectedView, self).dispatch(*args, **kwargs)


class BoardListView (ListView):
    model = Board

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BoardListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.request.user.boards.all()

    def get_context_data(self, **kwargs):
        context = super(BoardListView, self).get_context_data(**kwargs)
        context.update({
            'form': BoardForm(),
            #'user': User.objects.get(pk=self.request.user.id),
        })
        return context


class BoardDetailView(DetailView):
    model = Board

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BoardDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BoardDetailView, self).get_context_data(**kwargs)

        board = kwargs.get('object')
        user = self.request.user

        if not board.is_member(self.request.user):
            raise PermissionDenied

        context.update({
            'board': board,
            'is_owner':  board.is_owner(user)
        })
        return context


@login_required
def create(request):
    form = BoardForm(request.POST or None)
    if not form.is_valid():
        return render(request, 'boards/board_create.html', {'form': form})

    else:
        board = Board(owner=request.user, name=form.cleaned_data.get('name'))
        board.save()

        board.set_members_from_emails(form.cleaned_data.get('members'))
        board.save()

        return redirect(board.get_absolute_url())


@login_required
def update(request, board_id):
    board = get_object_or_404(Board, pk=board_id)
    if not board.is_member(request.user):
        raise PermissionDenied

    if request.method != 'POST':
        return redirect(board.get_absolute_url())

    updated = {}

    updated_name = request.POST.get('name') or None
    if isinstance(updated_name, unicode) and board.name != updated_name:
        board.name = updated_name
        updated['name'] = updated_name

    added_members = []
    for t in request.POST.getlist('add_members') or []:
        added_members += [x for x in re.split(r'[\n\r\s,]+', t) if is_email(x)]

    if added_members and all([isinstance(x, unicode) for x in added_members]):
        board.add_members_from_emails(added_members)
        updated['added_members'] = added_members

    removed_members = request.POST.getlist('remove_members') or []

    if removed_members and all([isinstance(x, unicode) and x.isdigit() for x in removed_members]):
        board.remove_members_from_ids(removed_members)
        updated['removed_members'] = removed_members

    if updated:
        board.save()

    return redirect(board.get_absolute_url())


@login_required
def leave(request, board_id):
    board = get_object_or_404(Board, pk=board_id)
    if not board.is_member(request.user):
        raise PermissionDenied

    if request.user == board.owner:
        board.delete()
    else:
        board.members.remove(request.user)
    return redirect('boards:list')

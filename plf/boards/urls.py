from django.conf.urls import patterns, url
from boards.views import BoardDetailView, BoardListView

urlpatterns = patterns(
    '',
    url(r'^$', BoardListView.as_view(), name='list'),
    url(r'^create/$', 'boards.views.create', name='create'),
    url(r'^(?P<pk>\d+)/$', BoardDetailView.as_view(), name='detail'),
    url(r'^(?P<board_id>\d+)/update/$', 'boards.views.update', name='update'),
    url(r'^(?P<board_id>\d+)/leave/$', 'boards.views.leave', name='leave'),
)
